package hw01;
import java.util.Random;
import java.util.random.RandomGenerator;
import java.util.Scanner;
public class Numbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Please enter your name: ");
        String Name = sc.nextLine();
        System.out.println("Let the game begin!");
        System.out.println("Enter your integer number from 0 to 100: ");
        int Number = sc.nextInt();

        Random Rand = new Random();
        int RandNumber = Rand.nextInt(101);
        while (Number != RandNumber) {
            if (Number > RandNumber && Number <= 100) {
                System.out.println("Your number is too big. Please, try again.");
                Number = sc.nextInt();
            }
            if (Number < RandNumber && Number >= 0) {
                System.out.println("Your number is too small. Please, try again.");
                Number = sc.nextInt();
            }
        }
        System.out.println("Congratulations, " + Name + "!");
    }
}